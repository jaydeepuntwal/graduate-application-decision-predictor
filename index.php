<?php

	include"connect.php";
	$selectdb = mysql_select_db("gradapp", $con);
	
	if(isset($_POST['Submit']))
	{
		
		$Gender = $_POST['Gender'];
		$Age_Group = $_POST['Age_Group'];
		$Gre_Type = $_POST['Gre_Type'];
		$Gre_Quant = $_POST['Gre_Quant'];
		$Gre_Verbal = $_POST['Gre_Verbal'];
		$Gre_Total;
		$Toefl = $_POST['Toefl'];
		$Undergrad = $_POST['Undergrad'];
		$UResult = $_POST['UResult'];
		$WorkEx = $_POST['WorkEx'];
		
		$AccUni = $_POST['UniversityA'];
		
		$Major = $_POST['SelMajor'];
		
		if($Major > 0){
		
			if(count($AccUni)>0){
			
				if($Gre_Type == 'Old'){
				
				// Convert to new and save in all including total
				include "Quant.php";
				include "Verbal.php";
				
				$Gre_Quant = getQuant($Gre_Quant);
				$Gre_Verbal = getVerbal($Gre_Verbal);
				$Gre_Total = $Gre_Verbal + $Gre_Quant;
			
				} else {
					$Gre_Total = $Gre_Verbal + $Gre_Quant;
				}
				
				
				if($Undergrad == 'Percentage'){
					$UResult = ($UResult / 20) - 1;
					
					if($UResult < 0){
						$UResult = 0;
					}
				
					
				} else if($Undergrad == 'GPA'){
					if($UResult > 4){
						$UResult = ($UResult * 4) / 10;
					}
				}
					
				
				
				if($selectdb){
					// Make Prediction
					header("Location: Predictor/index.php?Gender=".$Gender."&Age_Group=".$Age_Group."&Gre_Quant=".$Gre_Quant."&Gre_Verbal=".$Gre_Verbal."&Toefl=".$Toefl."&Undergrad_GPA=".$UResult."&WorkExYears=".$WorkEx."&Uni=".$AccUni."&Major=".$Major);
				}
				
			} else {
				echo '<script language="javascript">';
				echo 'alert("Please select atleast one University")';
				echo '</script>';
			}
		} else {
				echo '<script language="javascript">';
				echo 'alert("Please select a Major")';
				echo '</script>';
		}
	}

?>


<!DOCTYPE html>
<html>
<head>
	<meta name=viewport content="user-scalable=no,width=device-width" />
	<title>Graduate Appication Predictor</title>	
	
<link rel="stylesheet" href="//code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.css" />
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.js"></script>

<style type="text/css">

body,div
{
 font-family:'century gothic';
 font-weight:bold;
}

.ui-header .ui-title {
  overflow: visible !important;
  white-space: normal !important;
}

.ui-btn {
	text-overflow: inherit;
}

</style>

<script>

$( document ).on( "pagecreate", "#pagetwo", function() {
	$( "#autocompleteMajor" ).on( "filterablebeforefilter", function ( e, data ) {
		var $ul = $( this ),
			$input = $( data.input ),
			value = $input.val(),
			html = "";
		$ul.html( "" );
		if ( value && value.length > 2 ) {
			$ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
			$ul.listview( "refresh" );
			$.ajax({
				url: "getMajor.php?Search="+value,
				dataType: "json",
				crossDomain: true,
				data: {
					q: $input.val()
				}
			})
			.then( function ( response ) {
				$.each( response, function ( i, val ) {
					html += "<li><button onclick='addMajor("+ val['Major_Id'] + ",\"" + val['Major_Description'] + "\"); return false;' class='ui-btn ui-corner-all'>" + val['Major_Description'] + "</button></li>";
				});
				$ul.html( html );
				$ul.listview( "refresh" );
				$ul.trigger( "updatelayout");
			});
		}
	});
});


</script>



<script>

$( document ).on( "pagecreate", "#pagetwo", function() {
	$( "#autocompleteA" ).on( "filterablebeforefilter", function ( e, data ) {
		var $ul = $( this ),
			$input = $( data.input ),
			value = $input.val(),
			html = "";
		$ul.html( "" );
		if ( value && value.length > 2 ) {
			$ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
			$ul.listview( "refresh" );
			$.ajax({
				url: "getUniversity.php?Search="+value,
				dataType: "json",
				crossDomain: true,
				data: {
					q: $input.val()
				}
			})
			.then( function ( response ) {
				$.each( response, function ( i, val ) {
					html += "<li><button onclick='addAUni("+ val['University_Id'] + ",\"" + val['University_Name'] + "\",\"" + val['University_State'] +"\"); return false;' class='ui-btn ui-corner-all'>" + val['University_Name'] + ", " + val['University_State'] + "</button></li>";
				});
				$ul.html( html );
				$ul.listview( "refresh" );
				$ul.trigger( "updatelayout");
			});
		}
	});
});


</script>

<script>	

	function clear(){
		$("#autocompleteA").empty();
		$("#autocompleteMajor").empty();
	}
	
	function addMajor(Maj_Id, Maj_Name){
	
		if($("#"+Maj_Id).length == 0){
			var html = $("<li><input required='required' checked='true' type='radio' name='SelMajor' id='" + Maj_Id + "' value='" + Maj_Id + "'><label for='" + Maj_Id + "'>" + Maj_Name + "</label></li>");
			$("#SelMajor").append(html);
			
			clear();
		}
	}

	function addAUni(Uni_Id, Uni_Name, Uni_State){
	
		if($("#"+Uni_Id).length == 0){
			var html = $("<li><input checked='true' type='radio' name='UniversityA' id='" + Uni_Id + "' value='" + Uni_Id + "'><label for='" + Uni_Id + "'>" + Uni_Name + "</label></li>");
			$("#AcceptedUni").append(html);
			
			clear();
		}
	}

</script>


</head>
<body>

	<div data-role="page" id="pagetwo">

		<div data-role="navbar">
            <ul>
                <li><a href="survey.php" data-icon="grid">Survey</a></li>
                <li><a href="index.php" data-icon="star" class="ui-btn-active">Prediction</a></li>
            </ul>
        </div><!-- /navbar -->
		
		
		<div data-role="content">		
		
		<form action="index.php" method="post" data-ajax="false">
		
			<br>
			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup" data-type="horizontal">
					<legend>Choose your gender:</legend>
					<label for="Male">Male</label>
					<input type="radio" name="Gender" id="Male" value="0" required="required">
					<label for="Female">Female</label>
					<input type="radio" name="Gender" id="Female" value="1" required="required">	
				</fieldset>
			</div>
			<br>
			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup" data-type="horizontal">
					<legend>Age Group:</legend>
					<label for="1821">18-21</label>
					<input type="radio" name="Age_Group" id="1821" value="0" required="required">
					<label for="2224">22-24</label>
					<input type="radio" name="Age_Group" id="2224" value="1" required="required">
					<label for="2527">25-27</label>
					<input type="radio" name="Age_Group" id="2527" value="2" required="required">
					<label for="2830">28-30</label>
					<input type="radio" name="Age_Group" id="2830" value="3" required="required">	
					<label for="30p">30+</label>
					<input type="radio" name="Age_Group" id="30p" value="4" required="required">
				</fieldset>
			</div>
			<br>
			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup" data-type="horizontal">
					<legend>Choose Gre Type:</legend>
					<label for="Old">Old</label>
					<input type="radio" name="Gre_Type" id="Old" value="Old" required="required">
					<label for="New">New</label>
					<input type="radio" name="Gre_Type" id="New" value="New" required="required">
				</fieldset>
			</div>
			<br>
			<div data-role="fieldcontain">
				<label for="Gre_Quant">Gre Quantitative:</label>
				<input type="number" min=0 max=800 name="Gre_Quant" required="required" id="Gre_Quant">
				<label for="Gre_Verbal">Gre Verbal:</label>
				<input type="number" min=0 max=800 name="Gre_Verbal" required="required" id="Gre_Verbal">
			</div>
			<br>
			<div data-role="fieldcontain">
				<label for="Toefl">TOEFL Total:</label>
				<input type="number" min=0 max=120 name="Toefl" required="required" id="Toefl">
			</div>
			<br>
			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup" data-type="horizontal">
					<legend>Undergraduation Result Type:</legend>
					<label for="Percentage">Aggregate Percentage</label>
					<input type="radio" name="Undergrad" id="Percentage" value="Percentage" required="required">
					<label for="GPA">GPA</label>
					<input type="radio" name="Undergrad" id="GPA" value="GPA" required="required">
				</fieldset>
			</div>
			<br>
			<div data-role="fieldcontain">
				<label for="UResult">Undergraduation Total:</label>
				<input type="number" min=0 max=100 step ="any" required="required" name="UResult" id="UResult">
			</div>
			<br>
			<div data-role="fieldcontain">
				<label for="WorkEx">Work Experience (Number of Years - Enter 0 if none):</label>
				<input type="number" min=0 step ="any" required="required" name="WorkEx" id="WorkEx">
			</div>
			<br>
			
			
			<div data-role="fieldcontain">
			<fieldset data-role="controlgroup">
			<legend>Major (Master's):</legend><br>
				<ul id="autocompleteMajor" data-role="listview" data-filter="true" data-filter-reveal="true" data-filter-placeholder="Search Majors..." data-inset="true">			</fieldset>
			</div>			
			
			<div data-role="fieldcontain">
			<fieldset data-role="controlgroup">
			<legend>Selected Major:</legend><br>
				<ul id="SelMajor">
				</ul>
			</fieldset>
			</div>
			
			<div data-role="fieldcontain">
			<fieldset data-role="controlgroup">
			<legend>University:</legend><br>
				<ul id="autocompleteA" data-role="listview" data-filter="true" data-filter-reveal="true" data-filter-placeholder="Search Universities..." data-inset="true">
			</ul>
			</fieldset>
			</div>
			
			<div data-role="fieldcontain">
			<fieldset data-role="controlgroup">
			<legend>Selected Universities:</legend><br>
				<ul id="AcceptedUni">
				</ul>
			</fieldset>
			</div>		
		
			<br>
			<br>
			<center>
			<input type="Submit" name="Submit" value="Get Prediction" />			
			</center>
		</form>
	  </div>
	  
	</div> 
	
</body>
</html>