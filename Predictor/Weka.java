import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

public class Weka {
	static Instances testingInstances = null;

	public static Instances createTrainingDataset() {

		Instances trainingInstances = null;
		try {
			// creating the training dataset;
			CSVLoader trainingLoader = new CSVLoader();
			trainingLoader.setSource(new File("Student.csv"));
			trainingInstances = trainingLoader.getDataSet();
			trainingInstances.setClass(trainingInstances.attribute(0));
		} catch (IOException ex) {
			//Logger.getLogger(Weka.class.getName()).log(Level.SEVERE, null, ex);
		}
		return trainingInstances;
	}

	public static void trainClassifier(Instances trainingInstances) {
		try {

			Classifier classifier = (Classifier) new J48();

			// build the classifier for the training instances
			classifier.buildClassifier(trainingInstances);

			// Evaluating the classifer
			Evaluation eval = new Evaluation(trainingInstances);
			eval.evaluateModel(classifier, trainingInstances);

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream("JU.model"));
			out.writeObject(classifier);
			out.close();

		} catch (Exception ex) {
			//Logger.getLogger(Weka.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void makeInstance(int gender, int age_group, int gre_quant,
			int gre_verbal, int toefl, float undergrad, float workEx) {
		// Create the attributes
		// Gender,Age_Group,Gre_Quant,Gre_Verbal,Toefl,Undergrad_GPA,WorkExYears
		FastVector fvNominalVal = new FastVector(2);
		fvNominalVal.addElement("YES");
		fvNominalVal.addElement("NO");
		Attribute attribute1 = new Attribute("Result", fvNominalVal);
		Attribute attribute2 = new Attribute("Gender");
		Attribute attribute3 = new Attribute("Age_Group");
		Attribute attribute4 = new Attribute("Gre_Quant");
		Attribute attribute5 = new Attribute("Gre_Verbal");
		Attribute attribute6 = new Attribute("Toefl");
		Attribute attribute7 = new Attribute("Undergrad_GPA");
		Attribute attribute8 = new Attribute("WorkExYears");

		// Create list of attributes in one instance
		FastVector fvWekaAttributes = new FastVector(8);
		fvWekaAttributes.addElement(attribute1);
		fvWekaAttributes.addElement(attribute2);
		fvWekaAttributes.addElement(attribute3);
		fvWekaAttributes.addElement(attribute4);
		fvWekaAttributes.addElement(attribute5);
		fvWekaAttributes.addElement(attribute6);
		fvWekaAttributes.addElement(attribute7);
		fvWekaAttributes.addElement(attribute8);

		testingInstances = new Instances("Test relation", fvWekaAttributes, 10);

		// Set class index
		testingInstances.setClassIndex(0);
		Instance instance = new Instance(8);
		instance.setValue(attribute2, gender);
		instance.setValue(attribute3, age_group);
		instance.setValue(attribute4, gre_quant);
		instance.setValue(attribute5, gre_verbal);
		instance.setValue(attribute6, toefl);
		instance.setValue(attribute7, undergrad);
		instance.setValue(attribute8, workEx);
		testingInstances.add(instance);

	}

	public static void testingTheClassifier() {
		try {

			Classifier classifier;

			// load the model
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					"JU.model"));
			Object tmp = in.readObject();
			classifier = (Classifier) tmp;
			in.close();

			double[] resultDistribution = classifier
					.distributionForInstance(testingInstances.instance(0));
			double score = classifier.classifyInstance(testingInstances
					.instance(0));

			PrintStream fw = new PrintStream(new File("Output.txt"));

			fw.println(testingInstances.classAttribute().value((int) score));

			System.out.print(testingInstances.classAttribute().value(
					(int) score));
			System.out.println();

			for (int i = 0; i < resultDistribution.length; i++) {
				fw.println(Math.ceil(resultDistribution[i] * 100));
				System.out.println(Math.ceil(resultDistribution[i] * 100));
			}

			fw.close();

		} catch (Exception ex) {
			//Logger.getLogger(Weka.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void main(String[] args) throws Exception {

		trainClassifier(createTrainingDataset());
		makeInstance(Integer.parseInt(args[0]), Integer.parseInt(args[1]),
				Integer.parseInt(args[2]), Integer.parseInt(args[3]),
				Integer.parseInt(args[4]), Float.parseFloat(args[5]),
				Float.parseFloat(args[6]));
		testingTheClassifier();

	}

}